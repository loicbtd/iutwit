import { Global, Module } from '@nestjs/common';

import { Publication } from '../../iutwit/api/src/publication/Publication.entity';
import RepoService from './repo.service';
import { ThemePublication } from '../../iutwit/api/src/theme_publication/ThemePublication.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Utilisateur } from '../../iutwit/api/src/utilisateur/Utilisateur.entity';

@Global()
@Module({
  imports: [
    TypeOrmModule.forFeature([
      ThemePublication,
      Utilisateur,
      Publication,
    ]),
  ],
  providers: [RepoService],
  exports: [RepoService],
})
class RepoModule { }

export default RepoModule;