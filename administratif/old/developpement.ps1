$CONTAINER_NAME = "iutwit_dev_db"
$IMAGE_BUILD_TAG = $CONTAINER_NAME
$RUN_OPTIONS = ""
$DOCKERFILE_CONTEXT = "./iutwit/base_de_donnees/"

docker stop --time=0 $CONTAINER_NAME
docker rm --force $CONTAINER_NAME
docker rmi $(docker images --filter=dangling=true --quiet)
docker volume rm $(docker volume ls --filter=dangling=true --quiet)
docker build --pull --tag=$IMAGE_BUILD_TAG $DOCKERFILE_CONTEXT
docker run -p 3306:3306 --detach --name=$CONTAINER_NAME $RUN_OPTIONS $IMAGE_BUILD_TAG