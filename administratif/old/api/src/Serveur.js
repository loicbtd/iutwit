// import bodyParser from 'body-parser'
// import cors from 'cors'
import { GraphQLServer } from 'graphql-yoga'

const typeDefs = `
type Query {
  info: String!
}
`
const resolvers = {
  Query: {
    info: () => null
  }
}

const server = new GraphQLServer({
  typeDefs,
  resolvers,
})

server.start(() => console.log(`Server is running on http://localhost:4000`))