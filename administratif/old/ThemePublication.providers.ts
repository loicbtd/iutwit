import { Connection, Repository } from 'typeorm';

import { ThemePublication } from '../../iutwit/api/src/theme_publication/ThemePublication.entity';

export const ThemePublicationProviders = [
  {
    provide: 'THEME_PUBLICATION_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(ThemePublication),
    inject: ['DATABASE_CONNECTION'],
  },
];
