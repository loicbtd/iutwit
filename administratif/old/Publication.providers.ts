import { Connection, Repository } from 'typeorm';

import { Publication } from '../../iutwit/api/src/publication/Publication.entity';

export const PublicationProviders = [
  {
    provide: 'PUBLICATION_REPOSITORY',
    useFactory: (connection: Connection) =>
      connection.getRepository(Publication),
    inject: ['DATABASE_CONNECTION'],
  },
];
