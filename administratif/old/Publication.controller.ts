import { Controller, Get } from '@nestjs/common';

@Controller('publication')
export class PublicationController {
  @Get()
  findAll(): string {
    return 'This action returns publications';
  }
}