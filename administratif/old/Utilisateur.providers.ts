import { Connection, Repository } from 'typeorm';

import { Utilisateur } from '../../iutwit/api/src/utilisateur/Utilisateur.entity';

export const UtilisateurProviders = [
  {
    provide: 'UTILISATEUR_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Utilisateur),
    inject: ['DATABASE_CONNECTION'],
  },
];
