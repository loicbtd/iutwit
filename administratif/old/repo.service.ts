import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Publication } from '../../iutwit/api/src/publication/Publication.entity';
import { Repository } from 'typeorm';
import { ThemePublication } from '../../iutwit/api/src/theme_publication/ThemePublication.entity';
import { Utilisateur } from '../../iutwit/api/src/utilisateur/Utilisateur.entity';

@Injectable()
class RepoService {
  public constructor(
    @InjectRepository(ThemePublication) public readonly themePublicationRepo: Repository<ThemePublication>,
    @InjectRepository(Utilisateur) public readonly utilisateurRepo: Repository<Utilisateur>,
    @InjectRepository(Publication) public readonly publicationRepo: Repository<Publication>,
  ) {}
}

export default RepoService;