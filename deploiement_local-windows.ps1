# Le déploiement nécessite l'installation préalable de docker-desktop, docker-cli, docker-compose
# Le déploiement nécessite que docker-desktop soit démarré

cd iutwit
docker-compose build
docker-compose down --timeout 0
docker-compose up -d
Write-Host "NOTICE: L'application web est accessible a http://127.0.0.1:8100"
Write-Host "NOTICE: L'api est accessible a http://127.0.0.1:8101"
Write-Host "NOTICE: La base de donnees est accessible a http://127.0.0.1:8102"
cd ..