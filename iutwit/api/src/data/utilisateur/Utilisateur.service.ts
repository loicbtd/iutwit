import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Utilisateur } from './Utilisateur.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UtilisateurService {
  constructor(
    @InjectRepository(Utilisateur)
    private utilisateurRepository: Repository<Utilisateur>,
  ) {}

  async add(utilisateur: Utilisateur): Promise<Utilisateur> {
    return await this.utilisateurRepository.save(utilisateur);
  }

  async findAll(): Promise<Utilisateur[]> {
    return await this.utilisateurRepository.find({
      relations: ['publications'],
    });
  }

  async findOneById(id: number): Promise<Utilisateur> {
    return await this.utilisateurRepository.findOne({
      relations: ['publications'],
      where: {
        idUtilisateur: id,
      },
    });
  }

  async findOneByPseudonyme(pseudonyme: string): Promise<Utilisateur> {
    return await this.utilisateurRepository.findOne({
      relations: ['publications'],
      where: {
        pseudonyme: pseudonyme,
      },
    });
  }
  async update(utilisateur: Utilisateur): Promise<Utilisateur> {
    return await this.utilisateurRepository.save(utilisateur);
  }

  async remove(id: number): Promise<void> {
    await this.utilisateurRepository.delete(id);
  }
}