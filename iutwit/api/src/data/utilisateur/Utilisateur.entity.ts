import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Field, ID, ObjectType } from '@nestjs/graphql';

import { Publication } from '../publication/Publication.entity';

@ObjectType()
@Entity('utilisateur')
export class Utilisateur {
  @Field(type => ID)
  @PrimaryGeneratedColumn({ name: 'id_utilisateur' })
  idUtilisateur: number;

  @Field()
  @Column()
  pseudonyme: string;

  @Field()
  @Column()
  mot_de_passe: string;

  @Field(type => [Publication])
  @OneToMany(
    type => Publication,
    publication => publication.utilisateur,
  )
  publications: Publication[];
}