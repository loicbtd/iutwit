import { Module } from '@nestjs/common';
import { Publication } from './../publication/Publication.entity';
import { PublicationService } from '../publication/Publication.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Utilisateur } from './Utilisateur.entity';
import { UtilisateurResolver } from './Utilisateur.resolver';
import { UtilisateurService } from './Utilisateur.service';

@Module({
  imports: [TypeOrmModule.forFeature([Utilisateur])],
  providers: [UtilisateurResolver, UtilisateurService]
})
export class UtilisateurModule {}