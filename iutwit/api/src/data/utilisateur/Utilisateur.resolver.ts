import { Resolver, Args, Int, Query, Mutation } from '@nestjs/graphql';
import { Utilisateur } from './Utilisateur.entity';
import { UtilisateurService } from './Utilisateur.service';

@Resolver(of => Utilisateur)
export class UtilisateurResolver {
  constructor(private utilisateurService: UtilisateurService) {}

  @Mutation(returns => Utilisateur)
  createUtilisateur(
    @Args('pseudonyme', { type: () => String }) pseudonyme: string,
    @Args('mot_de_passe', { type: () => String }) mot_de_passe: string,
  ) {
    let utilisateur = new Utilisateur();
    utilisateur.pseudonyme = pseudonyme;
    utilisateur.mot_de_passe = mot_de_passe;
    return this.utilisateurService.add(utilisateur);
  }

  @Query(returns => [Utilisateur])
  readListUtilisateur() {
    return this.utilisateurService.findAll();
  }

  @Query(returns => Utilisateur)
  readUtilisateur(@Args('id', { type: () => Int }) id: number) {
    return this.utilisateurService.findOneById(id);
  }

  @Mutation(returns => Utilisateur)
  updateUtilisateur(
    @Args('id', { type: () => Int }) id: number,
    @Args('pseudonyme', { type: () => String }) pseudonyme: string,
    @Args('mot_de_passe', { type: () => String }) mot_de_passe: string,
  ) {
    let utilisateur = new Utilisateur();
    utilisateur.idUtilisateur = id;
    utilisateur.pseudonyme = pseudonyme;
    utilisateur.mot_de_passe = mot_de_passe;
    return this.utilisateurService.add(utilisateur);
  }

  @Mutation(returns => Int)
  deleteThemePublication(@Args('id', { type: () => Int }) id: number) {
    this.utilisateurService.remove(id);
    return id;
  }
}