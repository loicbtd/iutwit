import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Field, ID, ObjectType } from '@nestjs/graphql';

import { Publication } from '../publication/Publication.entity';

@ObjectType()
@Entity('theme_publication')
export class ThemePublication {
  @Field(type => ID)
  @PrimaryGeneratedColumn({ name: 'id_theme_publication' })
  idThemePublication: number;

  @Field()
  @Column()
  titre: string;

  @Field(type => [Publication])
  @ManyToMany(
    type => Publication,
    publication => publication.themes,
  )
  publications: Publication[];
}