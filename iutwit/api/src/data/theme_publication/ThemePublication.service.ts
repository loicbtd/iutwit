import { Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { ThemePublication } from './ThemePublication.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Publication } from '../publication/Publication.entity';

@Injectable()
export class ThemePublicationService {
  constructor(
    @InjectRepository(ThemePublication)
    private themePublicationRepository: Repository<ThemePublication>,
  ) {}

  async add(themePublication: ThemePublication): Promise<ThemePublication> {
    return await this.themePublicationRepository.save(themePublication);
  }

  async findAll(): Promise<ThemePublication[]> {
    return await this.themePublicationRepository.find({ relations: ['publications'] });
  }

  async findOneById(id: number): Promise<ThemePublication> {
    return await this.themePublicationRepository.findOne({
      relations: ['publications'],
      where: {
        idThemePublication: id,
      }
    });
  }

  async update(themePublication: ThemePublication): Promise<ThemePublication> {
    return await this.themePublicationRepository.save(themePublication);
  }

  async remove(id: number): Promise<void> {
    await this.themePublicationRepository.delete(id);
  }
}