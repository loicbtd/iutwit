import { ThemePublication } from './ThemePublication.entity';
import { ThemePublicationService } from './ThemePublication.service';
import { Resolver, Int, Args, Query, Mutation } from '@nestjs/graphql';

@Resolver(of => ThemePublication)
export class ThemePublicationResolver {
  constructor(private themePublicationService: ThemePublicationService) {}

  @Mutation(returns => ThemePublication)
  createThemePublication(@Args('titre', { type: () => String }) titre: string) {
    let themePublication = new ThemePublication();
    themePublication.titre = titre;
    return this.themePublicationService.add(themePublication);
  }

  @Query(returns => [ThemePublication])
  readListThemePublication() {
    return this.themePublicationService.findAll();
  }

  @Query(returns => ThemePublication)
  readThemePublication(@Args('id', { type: () => Int }) id: number) {
    return this.themePublicationService.findOneById(id);
  }

  @Mutation(returns => ThemePublication)
  updateThemePublication(
    @Args('id', { type: () => Int }) id: number,
    @Args('titre', { type: () => String }) titre: string,
  ) {
    let themePublication = new ThemePublication();
    themePublication.idThemePublication = id;
    themePublication.titre = titre;
    return this.themePublicationService.update(themePublication);
  }

  @Mutation(returns => Int)
  deleteThemePublication(@Args('id', { type: () => Int }) id: number) {
    this.themePublicationService.remove(id);
    return id;
  }
}
