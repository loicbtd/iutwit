import { Module } from '@nestjs/common';
import { Publication } from '../publication/Publication.entity';
import { PublicationService } from '../publication/Publication.service';
import { ThemePublication } from './ThemePublication.entity';
import { ThemePublicationResolver } from './ThemePublication.resolver';
import { ThemePublicationService } from './ThemePublication.service';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([ThemePublication])],
  providers: [ThemePublicationResolver, ThemePublicationService]
})
export class ThemePublicationModule {}