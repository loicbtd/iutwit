import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Publication } from './Publication.entity';
import { Repository } from 'typeorm';

@Injectable()
export class PublicationService {
  constructor(
    @InjectRepository(Publication)
    private readonly publicationRepository: Repository<Publication>,
  ) {}

  async add(publication: Publication): Promise<Publication> {
    return await this.publicationRepository.save(publication);
  }

  async findAll(): Promise<Publication[]> {
    return await this.publicationRepository.find({ relations: ['themes'] });
  }

  async findOneById(id: number): Promise<Publication> {
    return await this.publicationRepository.findOne({
      relations: ['themes', 'utilisateur'],
      where: {
        idPublication: id,
      },
    });
  }

  async update(publication: Publication): Promise<Publication> {
    return await this.publicationRepository.save(publication);
  }

  async remove(id: number): Promise<void> {
    await this.publicationRepository.delete(id);
  }
}