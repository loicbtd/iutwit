import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Field, ID, ObjectType } from '@nestjs/graphql';

import { ThemePublication } from './../theme_publication/ThemePublication.entity';
import { Utilisateur } from './../utilisateur/Utilisateur.entity';

@ObjectType()
@Entity('publication')
export class Publication {
  @Field(type => ID)
  @PrimaryGeneratedColumn({ name: 'id_publication' })
  idPublication: number;

  @Field(type => [ThemePublication])
  @ManyToMany(
    type => ThemePublication,
    themePublication => themePublication.publications,
  )
  @JoinTable({
    name: 'publication_theme_publication',
    joinColumn: {
      name: 'id_publication',
      referencedColumnName: 'idPublication',
    },
    inverseJoinColumn: {
      name: 'id_theme_publication',
      referencedColumnName: 'idThemePublication',
    },
  })
  themes: ThemePublication[];

  @Field()
  @Column()
  titre: string;

  @Field()
  @Column()
  contenu: string;

  @Field()
  @Column()
  date: Date;

  @Field()
  @Column()
  nombre_vues: number;

  @Field(type => Utilisateur)
  @ManyToOne(
    type => Utilisateur,
    utilisateur => utilisateur.publications,
  )
  utilisateur: Utilisateur;
}
