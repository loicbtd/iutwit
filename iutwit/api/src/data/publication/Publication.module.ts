import { Module } from '@nestjs/common';
import { Publication } from './Publication.entity';
import { PublicationResolver } from './Publication.resolver';
import { PublicationService } from './Publication.service';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Publication])],
  providers: [PublicationResolver, PublicationService,],
})
export class PublicationModule {}