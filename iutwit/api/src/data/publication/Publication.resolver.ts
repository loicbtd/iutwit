import { Publication } from './Publication.entity';
import { PublicationService } from './Publication.service';
import { Resolver, Args, Int, Query, Mutation } from '@nestjs/graphql';
import { ThemePublication } from '../theme_publication/ThemePublication.entity';

@Resolver(of => Publication)
export class PublicationResolver {
  constructor(private publicationService: PublicationService) {}

  @Mutation(returns => Publication)
  createPublication(
    @Args('id_themes', { type: () => [Int], nullable: true }) id_themes: [number],
    @Args('titre', { type: () => String }) titre: string,
    @Args('contenu', { type: () => String }) contenu: string,
    @Args('date', { type: () => Date }) date: Date,
    @Args('id_utilisateur', { type: () => Int }) id_utilisateur: number,
  ) {
    let publication = new Publication();
    let theme;
    for (let index = 0; index < id_themes.length; index++) {
      theme = new ThemePublication();
      theme.idThemePublication = id_themes[index];
      publication.themes.push(theme);
    }
    publication.titre = titre;
    publication.contenu = contenu;
    publication.date = new Date(date);
    publication.utilisateur.idUtilisateur = id_utilisateur;
    return this.publicationService.add(publication);
  }

  @Query(returns => [Publication])
  publicationList() {
    return this.publicationService.findAll();
  }

  @Query(returns => Publication)
  publication(@Args('id', { type: () => Int }) id: number) {
    return this.publicationService.findOneById(id);
  }

  @Mutation(returns => Publication)
  updatePublication(
    @Args('id', { type: () => Int }) id: number,
    @Args('id_themes', { type: () => [Int] }) id_themes: [number],
    @Args('titre', { type: () => String }) titre: string,
    @Args('contenu', { type: () => String }) contenu: string,
    @Args('date', { type: () => Date }) date: Date,
    @Args('id_utilisateur', { type: () => Int }) id_utilisateur: number,
  ) {
    let publication = new Publication();
    publication.idPublication = id;
    let theme;
    for (let index = 0; index < id_themes.length; index++) {
      theme = new ThemePublication();
      theme.idThemePublication = id_themes[index];
      publication.themes.push(theme);
    }
    publication.titre = titre;
    publication.contenu = contenu;
    publication.date = new Date(date);
    publication.utilisateur.idUtilisateur = id_utilisateur;
    return this.publicationService.add(publication);
  }

  @Mutation(returns => Int)
  deleteThemePublication(@Args('id', { type: () => Int }) id: number) {
    this.publicationService.remove(id);
    return id;
  }
}


