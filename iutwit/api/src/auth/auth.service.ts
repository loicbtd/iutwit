import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UtilisateurService } from '../data/utilisateur/Utilisateur.service';

@Injectable()
export class AuthService {
  constructor(
    private UtilisateurService: UtilisateurService,
    private jwtService: JwtService,
  ) {}

  async validateUser(pseudonyme: string, mot_de_passe: string): Promise<any> {
    const utilisateur = await this.UtilisateurService.findOneByPseudonyme(
      pseudonyme,
    );
    if (utilisateur && utilisateur.mot_de_passe === mot_de_passe) {
      const { mot_de_passe, ...resultat } = utilisateur;
      return resultat;
    }
    return null;
  }
  async login(user: any) {
    const payload = { username: user.username, sub: user.userId };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
