import { AppController } from './app.controller';
import { AuthModule } from '../auth/auth.module';
import { GraphQLModule } from '@nestjs/graphql';
import { Module } from '@nestjs/common';
import { PublicationModule } from '../data/publication/Publication.module';
import { ThemePublicationModule } from '../data/theme_publication/ThemePublication.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UtilisateurModule } from '../data/utilisateur/Utilisateur.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'iutwit',
      password: 'iutwit',
      database: 'iutwit',
      debug: false,
      autoLoadEntities: true,
      synchronize: true,
      logging: true,
    }),
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql',
      context: ({ req }) => ({ req })
    }),
    AuthModule,
    ThemePublicationModule,
    UtilisateurModule,
    PublicationModule,
  ],
  controllers: [AppController],
})
export class AppModule {}