import { ExecutionContext, createParamDecorator, UseGuards } from '@nestjs/common';

import { GqlExecutionContext, Resolver, Query } from '@nestjs/graphql';
import { Utilisateur } from '../data/utilisateur/Utilisateur.entity';
import { UtilisateurService } from '../data/utilisateur/Utilisateur.service';
import { GqlAuthGuard } from '../auth/gql-auth.guard';

export const CurrentUser = createParamDecorator(
  (data: unknown, context: ExecutionContext) => {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req.user;
  },
);

@Resolver('auth')
export class AppResolver {
  constructor(private utilisateurService: UtilisateurService) {}

  @Query(returns => Utilisateur)
  @UseGuards(GqlAuthGuard)
  whoAmI(@CurrentUser() utilisateur: Utilisateur) {
    return this.utilisateurService.findOneById(utilisateur.idUtilisateur);
  }
}