import { AppModule } from './app/app.module';
import { NestFactory } from '@nestjs/core';

async function start() {
  const app = await NestFactory.create(AppModule);
  await app.listen(4000);
  console.log('\nL\'api est accessible à http://127.0.0.1:4000');
}
start();