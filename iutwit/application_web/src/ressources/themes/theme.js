import { createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#65C1D2',
      light: '#8EE0E9',
      dark: '#336977',
    },
    secondary: {
      main: '#59CEC5',
      light: '#7FE7E2',
      dark: '#2D7268',
    },
  },
  typography: {
    useNextVariants: true,
  },
})

export default theme