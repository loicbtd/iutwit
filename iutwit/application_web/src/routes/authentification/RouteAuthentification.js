import 'ressources/styles/App.css'

import {
  Box,
  Button,
  Checkbox,
  Container,
  FormControlLabel,
  Grid,
  Link,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core'

import React from 'react'
import image_logo_iutwit_full from 'ressources/images/logo-iutwit-full.webp'
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  logo: {
  },
  screen: {

    height: '100vh'
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}))

function RouteAuthentification() {

  const classes = useStyles()
  const t = useTranslation().t

  return (
    <Container
      component='main'
      maxWidth='xs'
      justify='center'
      alignContent='center'>
      <Grid
        container
        justify='center'
        alignContent='center'
        className={classes.screen}>
        <Grid item align='center'>
          <Box mt={8}>
            <img
              height='200vh'
              src={image_logo_iutwit_full}
              alt='Logo IUTwit'
              className='image-sans-interaction'
            />
          </Box>
          <Typography component='h1' variant='h5'>
            {t('routes.authentification.accroche')}
          </Typography>
        </Grid>
        <Grid>
          <form className={classes.form} noValidate>
            <TextField
              variant='outlined'
              margin='normal'
              required
              fullWidth
              id='identifiant'
              label='Courriel / pseudonyme'
              name='identifiant'
              autoComplete='identifiant'
              autoFocus
            />
            <TextField
              variant='outlined'
              margin='normal'
              required
              fullWidth
              name='mot_de_passe'
              label='Mot de passe'
              type='password'
              id='mot_de_passe'
              autoComplete='mot_de_passe'
            />
            <FormControlLabel
              control={<Checkbox value='remember' color='primary' />}
              label='Rester connecté'
            />
            <Button
              type='submit'
              fullWidth
              variant='contained'
              color='primary'
              className={classes.submit}>
              Connexion
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href='#' variant='body2'>
                  Mot de passe oublié ?
                </Link>
              </Grid>
              <Grid item>
                <Link href='#' variant='body2'>
                  {'Pas de compte ? Créez-en un !'}
                </Link>
              </Grid>
            </Grid>
          </form>
        </Grid>
        <Box mt={8}>
          <Typography>Loïc BERTRAND 2020 © Tous droits réservés</Typography>
        </Box>
      </Grid>
    </Container>
  )
}

export default RouteAuthentification
