import 'ressources/styles/App.css'
import 'ressources/langages/i18n'

import { BrowserRouter, Route, Switch } from 'react-router-dom'
import React, { Suspense } from 'react'

import { MuiThemeProvider } from '@material-ui/core'
import RouteAuthentification from 'routes/authentification/RouteAuthentification'
import passport_local from 'passport-local'
import theme from 'ressources/themes/theme'

function App() {
  
  const utilisateur_authentifie = false

  const vue_authentifie = (
    <BrowserRouter>
      <Switch>
        <Route path='/messagerie'>
          Messagerie
        </Route>
        <Route path='/profil'>
          Profil
        </Route>
        <Route path='/'>
          Fil
        </Route>
      </Switch>
    </BrowserRouter>
  )

  const vue_non_authenfie = <RouteAuthentification />

  const Chargeur = () => (
    "Chargement..."
  )
  
  return (
    <MuiThemeProvider theme={theme}>
      <Suspense fallback={ <Chargeur /> }>
          {utilisateur_authentifie ? vue_authentifie : vue_non_authenfie}
      </Suspense>
    </MuiThemeProvider>
  )
}

export default App
