# Devlog - Historique du travail effectué

## 02/06/2020

* Formation NestJS + TypeORM + GraphQL
* Réalisation du début de l'API GraphQL


## 29/05/2020

* Formations/apprentissage NestJS
* Suite de la réalisation des livrables


## 07/05/2020

* Suite des cours sur GraphQL https://www.howtographql.com/graphql-js/2-a-simple-query/
* Déploiement de l'API GraphQL sur https://iutwut-api.projects.loicbertrand.net
* Déploiement de l'application web sur https://iutwut.projects.loicbertrand.net


## 06/05/2020

* Mise en place de la chaine de deploiement locale + de deploiement en production
* Définition de la charte graphique du projet
* Mise en place des routes
* Création de la page d'authentification
* Suite de formation sur GraphQL via https://www.howtographql.com/


## 05/05/2020

* Écriture des fichiers de configuration de déploiement automatique


## 04/05/2020

* Conception de la structure du projet


## 02/05/2020

* Étude de GraphQL


## 01/05/2020

* Création du repository et idéation pour définir le cadre général du cahier des charges