#!/bin/sh

# Le déploiement nécessite l'installation préalable de docker et docker-compose
# Le déploiement nécessite que docker soit activé (sudo systemctl start docker)

cd iutwit

echo "**** Création de l'environnement réseau ****" && \
  docker network inspect traefik > /dev/null || docker network create traefik

echo "**** Déploiement ****"
if docker-compose build && docker-compose down --timeout 0 && docker-compose up -d; then
  echo "NOTICE: Le déploiement a réussi !"
else 
  echo "ERREUR: Déploiement a échoué"
  exit 1
fi

printf "\n\n"
echo "NOTICE: L'application web est accessible à http://127.0.0.1:8100"
echo "NOTICE: L'api est accessible à http://127.0.0.1:8101"
echo "NOTICE: La base de données est accessible à http://127.0.0.1:8102"
printf "\n\n"

echo "Saisir l puis entrer pour suivre les logs des services en temps réel"
read reponse
[ "$reponse" = "l" ] && docker-compose logs -f

cd -

exit 0
