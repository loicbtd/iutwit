# IUTwit

IUTwit est un réseau social interne pour un IUT - Institut Universitaire de Technologie.

Ce projet a été réalisé dans le cadre d'un projet de fin d'étude de DUT Informatique.

Ce projet est une preuve de concept composée de 3 services :

* Une base de données MariaDB
* Une API GraphQL
* Une application web React.js


## Pour commencer

### Prérequis

#### Linux

* docker doit être installé et démarré
* docker-compose doit être installé


#### Windows

* docker-desktop, docker-cli et docker-compose doivent être installés
* docker-desktop doit être démarré


### Démarrage

* Executez le fichier en racine du projet intitulé "deploiement_local-linux.sh" ou "deploiement_local-windows.ps1" en fonction de votre système d'exploitation
* Rendez-vous sur http://127.0.0.1:8100 pour accéder à l'application web
* Rendez-vous sur http://127.0.0.1:8101 pour accéder à l'api
* Rendez-vous sur http://127.0.0.1:8102 pour accéder à la base de données


